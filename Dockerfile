FROM python:3

WORKDIR /app

RUN apt-get update && \
    apt-get install -y build-essential libpq-dev && \
    apt-get clean

RUN pip install --upgrade pip && \
    pip install Django

COPY . /app/

EXPOSE 8000