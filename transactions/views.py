import openai
from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework import generics
from .models import Transaction
from .serializers import TransactionSerializer
from django_filters import rest_framework as filters
from django.shortcuts import render

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse
from transactions.serializers import CustomUserCreationSerializer, LoginSerializer, UserSerializer
from django.contrib.auth import get_user_model

User = get_user_model()

openai.api_key = 'YOUR_OPENAI_API_KEY'


def index_view(request):
    return render(request, 'index.html')


class TransactionFilter(filters.FilterSet):
    start_date = filters.DateFilter(field_name="date", lookup_expr='gte')
    end_date = filters.DateFilter(field_name="date", lookup_expr='lte')

    class Meta:
        model = Transaction
        fields = ['start_date', 'end_date']


class TransactionListView(LoginRequiredMixin, generics.ListAPIView):
    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = TransactionFilter
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return self.queryset.filter(user=user)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def analyze_financial_data(request):
    user = request.user
    start_date = request.data.get('start_date')
    end_date = request.data.get('end_date')
    question = request.data.get('financial_question', '')

    if start_date and end_date:
        # If start_date and end_date are provided, filter transactions by date range
        transactions = Transaction.objects.filter(user=user, date__gte=start_date, date__lte=end_date)
    else:
        # If start_date or end_date is missing, use the full transaction history
        transactions = Transaction.objects.filter(user=user)

    transactions_text = "\n".join([f"{t.date}: {t.amount} USD on {t.category}" for t in transactions])

    prompt = f"""
    წარმოიდგინე რომ შენ ხარ ფინანსური კონსულტანტი და მრჩველი.
    შემდეგ ტრანზაქციებზე და ფინანსურ კითხვაზე დაყრდნობით მიუთითე მოკლე პასუხი.
    ტრანზაქციებში ნაჩვენებია შემდეგი ინფორმაცია:

    {transactions_text}

    ფინანსური შეკითხვა: {question}
    """

    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=prompt,
        max_tokens=150
    )

    return Response({'analysis': response.choices[0].text.strip()})


# api views


@api_view(['POST'])
@permission_classes([AllowAny])
def register(request):
    if request.method == 'POST':
        serializer = CustomUserCreationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            email = serializer.validated_data['email']
            password = serializer.validated_data['password1']
            user = authenticate(request, email=email, password=password)
            if user:
                login(request, user)
                return Response({'detail': 'User registered and logged in successfully'},
                                status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([AllowAny])
def user_login(request):
    if request.method == 'POST':
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            email = serializer.validated_data['email']
            password = serializer.validated_data['password']
            user = authenticate(request, email=email, password=password)
            if user is not None:
                login(request, user)
                if user.is_user:
                    return Response({'detail': 'User logged in successfully',
                                     'redirect_url': reverse('transactions:analyze-financial-data')},
                                    status=status.HTTP_200_OK)
                elif user.is_staff:
                    return Response({'detail': 'Admin logged in successfully', 'redirect_url': reverse('admin:index')},
                                    status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'Invalid email or password'}, status=status.HTTP_401_UNAUTHORIZED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def user_logout(request):
    if request.method == 'POST':
        logout(request)
        return Response({'detail': 'User logged out successfully'}, status=status.HTTP_200_OK)
