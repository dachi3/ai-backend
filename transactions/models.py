
from django.conf import settings
from django.db import models
import random


class Transaction(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    date = models.DateField()
    petrol_expense = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    food_expense = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    house_expense = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    education_expense = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    monthly_salary = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return f"{self.user} - {self.amount} on {self.date}"

    def save(self, *args, **kwargs):
        remaining_amount = self.amount - (
                self.petrol_expense + self.food_expense + self.house_expense + self.education_expense
        )
        expenses = [self.petrol_expense, self.food_expense, self.house_expense, self.education_expense]
        random.shuffle(expenses)
        for i in range(len(expenses)):
            if remaining_amount > 0:
                add_amount = min(remaining_amount, self.amount - sum(expenses))
                expenses[i] += add_amount
                remaining_amount -= add_amount
        self.petrol_expense, self.food_expense, self.house_expense, self.education_expense = expenses
        super().save(*args, **kwargs)
