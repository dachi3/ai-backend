from django.urls import path, include
from transactions.views import TransactionListView, analyze_financial_data, index_view, register, user_login, user_logout

app_name = 'transactions'
urlpatterns = [
    path('', index_view, name='index'),
    path('transaction-list/', TransactionListView.as_view(), name='transaction_list'),
    path('analyze/', analyze_financial_data, name='analyze-financial-data'),
    path('register/', register, name='register'),
    path('login/', user_login, name='login'),
    path('logout/', user_logout, name='logout'),

]
