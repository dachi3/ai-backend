from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from transactions.models import Transaction
from faker import Faker
import random

User = get_user_model()


class Command(BaseCommand):
    help = 'Generate fake transaction data with expense categories'

    def handle(self, *args, **kwargs):
        fake = Faker()

        users = User.objects.all()

        for user in users:
            monthly_salary = round(random.uniform(1000.0, 10000.0), 2)
            for _ in range(50):
                amount = round(random.uniform(5.0, 500.0), 2)

                petrol_expense = round(random.uniform(0.0, amount), 2)
                food_expense = round(random.uniform(0.0, amount - petrol_expense), 2)
                house_expense = round(random.uniform(0.0, amount - petrol_expense - food_expense), 2)
                education_expense = round(amount - petrol_expense - food_expense - house_expense, 2)

                transaction = Transaction.objects.create(
                    user=user,
                    amount=amount,
                    date=fake.date_this_year(),
                    petrol_expense=petrol_expense,
                    food_expense=food_expense,
                    house_expense=house_expense,
                    education_expense=education_expense,
                    monthly_salary=monthly_salary
                )

        self.stdout.write(self.style.SUCCESS('Fake data generated successfully.'))
